To create a Flutter app integrating the OkayCam iOS and Android native SDKs, follow these steps. 

### Prerequisites
- Flutter installed on your development machine.
- Xcode for iOS development.
- Android Studio for Android development.
- Access to the OkayCam SDKs for both iOS and Android.

### Step 1: Create a New Flutter Project
Open your terminal and run:
```bash
flutter create okaycam_integration
cd okaycam_integration
```

### Step 2: Add Platform-Specific Code
Flutter uses platform channels to integrate with native code. You will need to write native code in both Swift (iOS) and Kotlin/Java (Android).

#### iOS Integration

1. **Modify the Podfile**:
   Ensure your `ios/Podfile` is set up to use frameworks:
   ```ruby
   platform :ios, '12.0'

   use_frameworks!

   target 'Runner' do
    pod 'OkayCam', '1.1.28'
    
    post_install do |installer|
        installer.pods_project.targets.each do |target|
            if target.name == "CryptoSwift"
                puts "Enable module stability for CryptoSwift"
                target.build_configurations.each do |config|
                    config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES' 
                end
            end
        end
    end
   end
   ```

#### Android Integration

1. **Modify `build.gradle`**:
   Add the following to `android/app/build.gradle`:
   ```groovy
   dependencies {
        implementation('com.innov8tif.okaycam:OkayCam:1.2.6@aar') {
            transitive = true
        }
   }
   ```

### Step 3: Platform Channels in Flutter
Use platform channels to communicate between Flutter and the native code.

1. **Invoke Native Methods**:
   Ensure your native code handles these method calls.

   - **iOS** (`AppDelegate.swift`):
     ```swift
     import UIKit
     import Flutter
     import OkayCam

     @UIApplicationMain
     @objc class AppDelegate: FlutterAppDelegate {
     override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
     ) -> Bool {
        let licenseKey = ""
        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
        let channel = FlutterMethodChannel(name: "package_name/okayCam", binaryMessenger: controller.binaryMessenger)
        let mainColor = UIColor(red: 83.0/255.0, green: 196.0/255.0 , blue: 204.0/255.0, alpha: 1.0)
        let config = OkayCamConfig(viewController: controller)
        config.captureBtnColor = mainColor
        config.retakeBtnConfig.backgroundColor = UIColor(red: 241.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        config.retakeBtnConfig.contentColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        
        config.confirmBtnConfig.backgroundColor = mainColor
        config.confirmBtnConfig.contentColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        config.imageQuality = 0.7
        config.width = 250
        config.crop = true
        
        config.captureConfigPair = CaptureConfigPair(
            firstPhoto: OkayCamCaptureConfig(
                timeOut: 3,
                onFlash: false,
                outputPath: nil
            ),
            secondPhoto: nil
        )
        
        config.timer.backgroundColor = mainColor.withAlphaComponent(0.3)
        config.timer.textColor = UIColor.white

        config.bottomLabel.text = "Tap the screen to focus"
        config.bottomLabel.color =  mainColor
        config.bottomLabel.size = 14

        config.frame.size = CGSize(width: UIScreen.main.bounds.size.width / 1.4, height: UIScreen.main.bounds.size.width/2)
        
        channel.setMethodCallHandler { [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "idCapture":
                OkayCamDoc.start(okayCamConfig: config, license: licenseKey) { [weak self] filePath, error in
                    if let filePath = filePath, filePath.count > 0 {
                        print(filePath)
                        result(filePath[0].path)
                    }
                }
            default:
                result(FlutterMethodNotImplemented)
            }
        }
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
        }
     }

     ```

   - **Android** (`MainActivity.kt`):
     ```
     import android.os.Bundle
     import android.util.Log
     import android.widget.Toast
     import androidx.core.content.ContextCompat
     import com.innov8tif.okaycam.cam.OkayCamDoc
     import com.innov8tif.okaycam.config.CaptureConfigPair
     import com.innov8tif.okaycam.config.OkayCamCaptureConfig
     import com.innov8tif.okaycam.config.OkayCamConfig
     import io.flutter.embedding.android.FlutterActivity
     import io.flutter.plugin.common.MethodChannel

     class MainActivity: FlutterActivity() {
        private val CHANNEL = "com.innov8tif.okaycamflutter/okayCam"
        private val LICENSE_KEY = ""

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            val config = OkayCamConfig.init(this).apply {
                width = 2000
                crop = true
                imageQuality = 0.7f
                showOverlay = true
                captureBtnColor = ContextCompat.getColor(this@MainActivity, R.color.colorAccent)
                showPreviewInstruction = false
                captureConfig = CaptureConfigPair(
                    first = OkayCamCaptureConfig(0, false, null),
                    second = OkayCamCaptureConfig(5, true, null),
                )
            }

            flutterEngine?.dartExecutor?.binaryMessenger?.let {
                MethodChannel(it, CHANNEL).setMethodCallHandler { call, result ->
                    when (call.method) {
                        "idCapture" -> {
                            OkayCamDoc.start(this, LICENSE_KEY, config) { success, images, exception ->
                                Log.d("TAG", "status => $success")
                                Log.d("TAG", "images => $images")

                                if (success) {
                                    result.success(images?.get(0))
                                } else {
                                    Log.i("OkayCam", "${exception?.message}")
                                    Toast.makeText(
                                        this@MainActivity,
                                        "${exception?.message}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                        else -> result.notImplemented()
                    }
                }
            }
        }
     }
     ```

### Step 4: Use OkayCam in Flutter
In your Flutter UI, you can now use the `MethodChannel` to interact with OkayCam SDK:
```dart
import 'package:flutter/material.dart';
import 'okaycam.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: OkayCamScreen(),
    );
  }
}

class OkayCamScreen extends StatefulWidget {
  @override
  _OkayCamScreenState createState() => _OkayCamScreenState();
}

class _OkayCamScreenState extends State<MyHomePage> {
  static const okayCamChannel =
      MethodChannel('com.innov8tif.okaycamflutter/okayCam');
  File? _idFront;

  _takeIDPhoto() async {
    try {
      final String idPath = await okayCamChannel.invokeMethod('idCapture');
      if (idPath.isNotEmpty) {
        setState(() {
          _idFront = File(idPath);
        });
      }
    } on PlatformException catch (e) {
      debugPrint('error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Visibility(
          visible: (_idFront != null),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 16.0),
                height: 275,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: _idFront != null
                      ? DecorationImage(
                          image: FileImage(_idFront!),
                          fit: BoxFit.fill,
                        )
                      : null,
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _takeIDPhoto,
        tooltip: 'Take Photo',
        child: const Icon(Icons.add),
      ),
    );
  }
}
```

This setup ensures that the Flutter app can interact with the native OkayCam SDK for both iOS and Android. For more details, you can refer to the [OkayCam SDK documentation](https://api2-ekycapis.innov8tif.com/okaycam-mobile-sdk/okaycam-mobile-sdk-all/getting-started-with-okaycam).