package com.innov8tif.okaycamflutter.okaycam_flutter_demo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.innov8tif.okaycam.cam.OkayCamDoc
import com.innov8tif.okaycam.config.CaptureConfigPair
import com.innov8tif.okaycam.config.OkayCamCaptureConfig
import com.innov8tif.okaycam.config.OkayCamConfig
import io.flutter.embedding.android.FlutterActivity
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    private val CHANNEL = "com.innov8tif.okaycamflutter/okayCam"
    private val LICENSE_KEY = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val config = OkayCamConfig.init(this).apply {
            width = 2000
            crop = true
            imageQuality = 0.7f
            showOverlay = true
            captureBtnColor = ContextCompat.getColor(this@MainActivity, R.color.colorAccent)
            showPreviewInstruction = false
            captureConfig = CaptureConfigPair(
                first = OkayCamCaptureConfig(0, false, null),
                second = OkayCamCaptureConfig(5, true, null),
//                second = null
            )
        }

        flutterEngine?.dartExecutor?.binaryMessenger?.let {
            MethodChannel(it, CHANNEL).setMethodCallHandler { call, result ->
                when (call.method) {
                    "idCapture" -> {
                        OkayCamDoc.start(this, LICENSE_KEY, config) { success, images, exception ->
                            Log.d("TAG", "status => $success")
                            Log.d("TAG", "images => $images")

                            if (success) {
                                result.success(images?.get(0))
                            } else {
                                Log.i("OkayCam", "${exception?.message}")
                                Toast.makeText(
                                    this@MainActivity,
                                    "${exception?.message}",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                    else -> result.notImplemented()
                }
            }
        }
    }
}
