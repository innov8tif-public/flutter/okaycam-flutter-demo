import UIKit
import Flutter
import OkayCam

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      let licenseKey = "2xdNOk6kijI_P8Vgm0UX44bXCJF5Y7t9dV_ituSyH-0E9zruAfESznKbBLeBjc0qlM75RrCrIk0ICYr82FyekQ"
      let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
      let channel = FlutterMethodChannel(name: "com.innov8tif.okaycamflutter/okayCam", binaryMessenger: controller.binaryMessenger)
      let mainColor = UIColor(red: 83.0/255.0, green: 196.0/255.0 , blue: 204.0/255.0, alpha: 1.0)
      let config = OkayCamConfig(viewController: controller)
      config.captureBtnColor = mainColor
      config.retakeBtnConfig.backgroundColor = UIColor(red: 241.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
      config.retakeBtnConfig.contentColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
    
      config.confirmBtnConfig.backgroundColor = mainColor
      config.confirmBtnConfig.contentColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
      config.imageQuality = 0.7
      config.width = 250
      config.crop = true
    
      config.captureConfigPair = CaptureConfigPair(
        firstPhoto: OkayCamCaptureConfig(
            timeOut: 3,
            onFlash: false,
            outputPath: nil
        ),
        secondPhoto: nil
      )
    
      config.timer.backgroundColor = mainColor.withAlphaComponent(0.3)
      config.timer.textColor = UIColor.white

      config.bottomLabel.text = "Tap the screen to focus"
      config.bottomLabel.color =  mainColor
      config.bottomLabel.size = 14

      config.frame.size = CGSize(width: UIScreen.main.bounds.size.width / 1.4, height: UIScreen.main.bounds.size.width/2)
      
      channel.setMethodCallHandler { [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          switch call.method {
          case "idCapture":
              OkayCamDoc.start(okayCamConfig: config, license: licenseKey) { [weak self] filePath, error in
                  if let filePath = filePath, filePath.count > 0 {
                      print(filePath)
                      result(filePath[0].path)
                  }
              }
          default:
              result(FlutterMethodNotImplemented)
          }
      }
      
      GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
